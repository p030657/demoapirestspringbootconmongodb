package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    @Autowired
    ServicioCliente servicioCliente;
    @Autowired
    ServicioCuenta servicioCuenta;
    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    };

    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);
        } catch(Exception x) {
            System.err.println(x);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final var cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{Cuenta}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento, @PathVariable String Cuenta) {
        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            if (!cliente.codigosCuentas.contains(Cuenta))
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);

            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(Cuenta);
            return ResponseEntity.ok(cuenta);

        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{Cuenta}")
    public ResponseEntity eliminarCuentaCliente(@PathVariable String documento, @PathVariable String Cuenta) {
        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(Cuenta);
            if(cliente.codigosCuentas.contains(Cuenta)) {
                cuenta.estado = "DESACTIVADO";
                cliente.codigosCuentas.remove(Cuenta);
            }
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.noContent().build();
    }
}
