package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {

    public List<Cliente> obtenerClientes(int pagina, int cantidad);

    public void insertarClienteNuevo(Cliente cliente);

    public Cliente obtenerCliente(String documento);

    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);

    public void borrarCliente(String documento);


    public void agregarCuentaCliente(String documento, String numeroCuenta);

    public List<String> obtenerCuentasCliente(String documento);
}