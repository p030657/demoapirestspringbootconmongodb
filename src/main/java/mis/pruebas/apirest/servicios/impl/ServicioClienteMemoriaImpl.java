package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//@Service
public class ServicioClienteMemoriaImpl implements ServicioCliente {

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        List<Cliente> clientesContados = List.copyOf(this.clientes.values());
        int indiceInicial = pagina*cantidad;
        int indiceFinal = indiceInicial+cantidad;
        if (indiceFinal > clientesContados.size()){
            indiceFinal= clientesContados.size();
        }
        return clientesContados.subList(indiceInicial , indiceFinal);
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        if (!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        return this.clientes.get(documento);
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if (!this.clientes.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente: " + cliente.documento);

        // No dejo reemplazar las cuentas del cliente.
        final var cuentasCliente = obtenerCuentasCliente(cliente.documento);
        cliente.codigosCuentas = cuentasCliente;

        // "Piso" el objeto en el hashmap.
        this.clientes.replace(cliente.documento, cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != existente.edad)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientes.replace(existente.documento, existente);
    }

    @Override
    public void borrarCliente(String documento) {
        if (!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        this.clientes.remove(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.codigosCuentas.add(numeroCuenta);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = this.obtenerCliente(documento);
        return cliente.codigosCuentas;
    }
}