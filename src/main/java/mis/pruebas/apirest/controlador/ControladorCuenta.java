package mis.pruebas.apirest.controlador;


import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
@CrossOrigin(origins ={"http://localhost:9000"}, maxAge = 3600)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}
    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta){this.servicioCuenta.insertarCuentaNueva(cuenta);}

    @GetMapping("/{Nro}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        return this.servicioCuenta.obtenerCuenta(numero);
    }

    @PutMapping("/{Nro}")
    public void reemplazarunaCuenta(@PathVariable("Nro") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        cuenta.numero = ctanumero;
        this.servicioCuenta.guardarCuenta(cuenta);
    }

    @PatchMapping("/{Nro}")
    public void emparcharunaCuenta(@PathVariable("Nro") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numero = ctanumero;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    @DeleteMapping("/{Nro}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {
        }
    }

}
