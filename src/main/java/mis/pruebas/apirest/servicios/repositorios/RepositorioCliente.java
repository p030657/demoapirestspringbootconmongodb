package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCliente extends MongoRepository<Cliente, String> {

}
